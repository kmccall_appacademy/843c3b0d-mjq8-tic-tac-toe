require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
class Game

  attr_accessor :current_player, :board, :player_one, :player_two

  def initialize (player_one, player_two)
    @board = Board.new
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
  end


  def play
    @current_player.display(@board)

    until board.over?
      play_turn
    end
    if board.over?
        @player_one.display(@board) if current_player == @player_two
        if board.winner.to_s == "X"
          puts "Congrats!! " + @player_one.name + " is the winner!"
        elsif board.winner.to_s == "O"
          puts "Darn :( " + @player_two.name + " is the winner!"
        else
          puts "~Tie game~"
        end
    end
  end

  def play_turn
    begin
      @board.place_mark(@current_player.get_move, @current_player.mark)
    rescue
      puts "That spot is taken!"
     retry
    end
    switch_players!
    current_player.display(board)
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    elsif @current_player == @player_two
      @current_player = @player_one
    end
  end

end

if $PROGRAM_NAME == __FILE__
  puts "Welcome to Tic Tac Toe!"
  p "What is your name?"
  name = gets.chomp
  human = HumanPlayer.new(name)
  comp = ComputerPlayer.new("Rival")
  new_game = Game.new(human, comp)
  new_game.play
end
