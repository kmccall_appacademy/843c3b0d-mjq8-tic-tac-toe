class ComputerPlayer

  attr_accessor :mark, :name
  attr_reader :board

  def initialize (name)
    @name = name
    #@board = Game.board
    @mark = :O
  end
  def name
    @name
  end

  def display(board)
    @board = board
  end

  def board
    @board
  end

  def mark
    @mark
  end

  def get_move
    avail_moves = []
    (0..2).each do |row|
      (0..2).each do |column|
        avail_moves << [row, column] if board.empty?([row, column])
      end
    end
    avail_moves.each do |move|
      if winning_move?(move)
        return move
      end
    end
        avail_moves.sample
  end

  def winning_move?(move)
      board[move] = mark
      if board.winner == mark
        board[move] = nil
        return true
      else
        board[move] = nil
        return false
      end
  end

end
