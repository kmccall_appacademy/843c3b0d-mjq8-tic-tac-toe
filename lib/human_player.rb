class HumanPlayer

  attr_reader :name
  attr_accessor :mark, :board

  def initialize(name)
    @name = name
    @mark = :X
  end

  def name
    @name
  end

  def mark
    @mark
  end

  def display(board)
    row0 = " |"
    (0..2).each do |col|
      row0 << (board.empty?([0, col]) ? "  |" : " " + board[[0, col]].to_s + " |")
    end
    row1 = " |"
    (0..2).each do |col|
      row1 << (board.empty?([1, col]) ? "  |" : " " + board[[1, col]].to_s + " |")
    end
    row2 = " |"
    (0..2).each do |col|
      row2 << (board.empty?([2, col]) ? "  |" : " " + board[[2, col]].to_s + " |")
    end
    puts row0
    puts "___________"
    puts row1
    puts "___________"
    puts row2
  end

  def get_move
    puts "Where do you want to move in the form '0, 0'?:\n"
    move = gets.chomp
    pos1 = move[0].to_i
    pos2 = move[-1].to_i
    pos = [pos1,pos2]
  end
end
